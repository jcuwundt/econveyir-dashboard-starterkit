import React from 'react';
import ReactDOM from 'react-dom';
import Example from './';


describe('Example', ()=> {
  it('Added <Example /> ', ()=> {
    const div = document.createElement('div');
    ReactDOM.render(<Example />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

});
